(deffacts Initial-state-objects-rooms-zones-actors

; Objects definitions
	( item (type Objects) (name deposit)(room corridor)(image table)( attributes no-pick brown)(pose 5.00000 10.00000 0.0)) ;<---cor
	( item (type Objects) (name storage)(room kitchen)(image table)( attributes no-pick brown)(pose 18.00000 14.000000 0.0)) ;<---kit
  ( item (type Objects) (name stser)(room service)(image table)( attributes no-pick brown)(pose 16.50000 4.550000 0.0)) ;<---ser
	( item (type Objects) (name bookcase)(room studio)(image table)( attributes no-pick brown)(pose 4.00000 3.50000 0.0)) ;<---studio
	( item (type Objects) (name deposit)(room deposit)(image table)( attributes no-pick brown)(pose 45.0000 15.00 0.0)) ;<---deposit


	( item (type Objects) (name A)(room corridor)(zone deposit)(image A)(attributes none)(pose 0.3 1.1 0.0))


	( item (type Objects) (name SU)(room corridor)(zone deposit)(image SU)(attributes pick)(pose 0.25 1.1 0.0))
	( item (type Objects) (name M)(room corridor)(zone deposit)(image M)(attributes pick)(pose 0.2 1.1 0.0))
	( item (type Objects) (name SO)(room corridor)(zone deposit)(image SO)(attributes pick)(pose 0.3 1.0 0.0))
	( item (type Objects) (name P)(room corridor)(zone deposit)(image P)(attributes pick)(pose 0.25 1.0 0.0))
	( item (type Objects) (name SH)(room corridor)(zone deposit)(image SH)(attributes pick)(pose 0.2 1.0 0.0))
	( item (type Objects) (name B)(room studio)(zone bookcase)(image B)(attributes pick)(pose 0.17 0.18 0.0))
	( item (type Objects) (name H)(room deposit)(zone stdep)(image H)(attributes pick)(pose 0.2 1.6 0.0))
	( item (type Objects) (name freespace)(room any)(zone any)(image none)(attributes none)(pose 0.0 0.0 0.0))

; Rooms definitions
	( Room (name corridor)(zone deposit)(zones dummy1 dummy2 deposit)(center 0.45 1.05))
	( Room (name kitchen)(zone fridge)(zones dummy1 dummy2 storage fridge)(center 1.5 1.4))
  ( Room (name service)(zone stser)(zones dummy1 dummy2 stser)(center 1.44 0.71))
	( Room (name bedroom)(zone bed)(zones dummy1 dummy2)(center 1.05 0.60))
  ( Room (name deposit)(zone stdep)(zones dummy1 dummy2 stdep)(center 0.65 1.60))
  ( Room (name studio)(zone bookcase)(zones dummy1 dummy2)(center 0.40 0.50 ))

; Robots definitions
	( item (type Robot) (name robot)(zone frontexit)(pose 4.00 5.00 0.0))

;Human definitions
	;( item (type Human) (name Mother)(room studio)(zone dummy1)(objects dummy1)(pose 3.00 6.00 0.00)(locations bedroom kitchen))
	;( item (type Human) (name Kid)(room bedroom)(zone dummy1)(objects dummy1)(pose 10.00 4.50 0.00)(locations bedroom kitc	hen))
	( item (type Human) (name MOTH )(room studio)(zone dummy1)(pose 0.20 0.70 0.00))
	( item (type Human) (name KID)(room bedroom)(zone dummy1)(pose 0.9 0.7 0.00))

; Furniture definitions
	( item (type Furniture) (name fridge)(zone kitchen)(image table)( attributes no-pick brown)(pose 1.5 1.4 0.0))
  ( item (type Furniture) (name bed)(zone bedroom)(image table)( attributes no-pick brown)(pose 1.00 2.00 0.0))
	( item (type Furniture) (name bookcase)(zone studio)(image table)( attributes no-pick brown)(pose 4.00 1.50 0.0))
	( item (type Furniture) (name table)(zone deposit)(image table)( attributes no-pick brown)(pose 4.50 1.80 0.0))
	( item (type Furniture) (name table)(zone service)(image table)( attributes no-pick brown)(pose 16.50 2.00 0.0))

; Doors definitions
	( item (type Door) (name outsidedoor) (status closed) )
	( Arm (name left))

;stacks definitions

				(stack corridor deposit A SU M)                  ;Dicta el orden de las acciones
				(stack corridor deposit SO P SH)
        (stack deposit stdep H)
				(stack studio bookcase B)


        (real-stack corridor deposit A SU M)
				(real-stack corridor storage SO P SH)
        (real-stack deposit stdep H)
        (real-stack studio bookcase B)


	(goal-stack 2 service stser SO P SH)
	(goal-stack 1 kitchen fridge SU A M)

	(give 3 KID B)
	(give 4 MOTH H)

    (plan (name cubes) (number 0)(duration 0))
)
