#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <queue>
#include <vector>
#include <ros/package.h>
#define MUM_NODES 250

typedef struct conection_
{
   int node;
   float cost;
}  conection;

typedef struct nodo_
{
   char flag;
   int num_node;
   float x;
   float y;
   int num_conections;
   conection conections[100];
   int parent;
   double acumulado;
}  nodo;

struct PQ
{
    double cost;
    int node;

    PQ(double c, int n) : cost(c), node(n){}

    bool operator<(const struct PQ& other) const
    {
        return cost >= other.cost;
    }
    bool operator==(const struct PQ& other) const
    {
        return cost == other.cost;
    }
    bool operator>(const struct PQ& other) const
    {
        return cost <= other.cost;
    }
};

nodo nodes[MUM_NODES];
int num_nodes = 0;

// it reads the file that conteins the environment description
int read_nodes(char *file)
{
   FILE *fp;
   char data[ 100 ];
   int i=0;
   int flg = 0;
   float tmp;
   float dimensions_room_x,dimensions_room_y;
   int node_index,node_connection,cost;

   fp = fopen(file,"r"); 
    
   if( fp == NULL )
   {
      sprintf(data, "File %s does not exists\n", file);
      printf("File %s does not exists\n", file);
      return(0);
   }

   while( fscanf(fp, "%s" ,data) != EOF)
   {
      if( strcmp(";(", data ) == 0 )
      {
         flg = 1;
         while(flg)
         {
            if(  0 < fscanf(fp, "%s", data));
            sscanf(data, "%f", &tmp);
            if(strcmp(")", data) == 0) flg = 0;
         }
      }
      else if((strcmp("node", data ) == 0) && ( flg == 0 ) )
      {
         if(  0 < fscanf(fp, "%s", data));
         nodes[i].num_node = atoi(data);
         if(  0 < fscanf(fp, "%s", data));
         nodes[i].x = atof(data);
         if(  0 < fscanf(fp, "%s", data));
         nodes[i++].y = atof(data);
      }
      else if((strcmp("connection", data ) == 0) && ( flg == 0 ) )
      {
         if(  0 < fscanf(fp, "%s", data));
         node_index = atoi(data);
         
         if(  0 < fscanf(fp, "%s", data));
         node_connection = atoi(data);

         nodes[node_index].conections[nodes[node_index].num_conections].node = node_connection;

         if(  0 < fscanf(fp, "%s", data));
         nodes[node_index].conections[nodes[node_index].num_conections].cost = atof(data);
         nodes[node_index].num_conections++;
      }
   }
   fclose(fp);
   return i;
}

void dijkstra_algorithm(int D ,int L)
{
   /*
      D = Nodo Inicial
      L = Nodo Final
      Y = Totalmente expandido
      N = Nuevo (Nodo sin padre ni acumulado)
      P = Nodo que no es ni Y ni N   (Parcialmente expandido)

      Video explicativo https://www.youtube.com/watch?v=LLx0QVMZVkk
   */

   int menor,flagOnce;
   int contador = 0;
   int j;
   conection next_node;
   double next_cost=0;
   std::priority_queue <PQ> cola;
   std::priority_queue <PQ> cola_aux;
   nodes[D].acumulado = 0;
   while( D!=L && nodes[L].flag != 'Y' )
   {  
      printf("Desde %d:\n",D);
      for(j = 0 ; j < nodes[D].num_conections; j++){
         next_node=nodes[D].conections[j];
         next_node.cost=sqrt(pow( nodes[next_node.node].x - nodes[D].x ,2) + pow( nodes[next_node.node].y - nodes[D].y ,2));
         //printf("%d\t%1.5f\n",next_node.node,next_node.cost);
         next_cost=nodes[D].acumulado + next_node.cost;
         if( nodes[ next_node.node].flag == 'N')
         {
            nodes[next_node.node].acumulado = next_cost;
            nodes[next_node.node].parent = D;
            nodes[next_node.node].flag = 'P';
            //agrega a cola de prioridad
            //printf("%f   %d\n",next_cost ,next_node.node);
            cola.push(PQ(next_cost ,next_node.node));
         
         }else if( nodes[next_node.node].flag == 'P' ){
            if( nodes[next_node.node].acumulado > next_cost){
               nodes[next_node.node].acumulado = next_cost;
               nodes[next_node.node].parent = D;
               while(!cola.empty()){
                  if(cola.top().node==next_node.node){
                     cola.pop();
                     cola_aux.push(PQ(next_cost ,next_node.node));
                  }else{
                     cola_aux.push(cola.top());
                     cola.pop();
                  }
               }
               cola.swap(cola_aux);
            }
         }
      }
      /*
      while(!cola.empty()){
         printf("%d:%f\t",cola.top().node,cola.top().cost);
         cola_aux.push(cola.top());
         cola.pop();
      }
      cola.swap(cola_aux);
      printf("\n");
      */
      
      nodes[D].flag = 'Y';      
      if(cola.empty()){
         printf("Error, cola vacia.\n");
      }else{
         D = cola.top().node;
         cola.pop();
      }
   }
}

void printNode(int i) // use it for debug
{
   printf("\n\n");
      printf("# %d  x   %f y %f\n",nodes[i].num_node,nodes[i].x,nodes[i].y );
      printf("flag: %c parent: %d   acumulado: %f  \n",nodes[i].flag,nodes[i].parent,nodes[i].acumulado  );
      printf("num_conections %d \n",nodes[i].num_conections);
      for(int j=0 ; j < nodes[i].num_conections; j++  )
         printf(     "%d  %f \n",nodes[i].conections[j].node,nodes[i].conections[j].cost );
}

int dijkstra(float rx ,float ry ,float lx ,float ly, char *world_name,step *steps )
{
   char archivo[150];
   float distStart=0,distGoal=0;
   int i;
   int start = 0;
   int goal = 0;
   int padre;
   std::string paths = ros::package::getPath("simulator");
   strcpy(archivo,paths.c_str());
   strcat(archivo,"/src/data/");
   strcat(archivo,world_name);
   strcat(archivo,"/");
   strcat(archivo,world_name);
   strcat(archivo,".top");

   for(i = 0; i < MUM_NODES; i++)
   {
      nodes[i].flag='N';
      nodes[i].num_conections = 0;
      nodes[i].parent = -1;
      nodes[i].acumulado = 0;
   }
 
   num_nodes=read_nodes(archivo); // Se lee el arcivo .top

   distStart=sqrt( pow( nodes[start].x - rx ,2) + pow( nodes[start].y - ry ,2)) ;
   distGoal=sqrt(pow( nodes[goal].x - lx ,2) + pow( nodes[goal].y - ly ,2) );

   for(i = 1; i < num_nodes; i++)
   {
   		if(sqrt(pow( nodes[i].x - rx ,2) + pow( nodes[i].y - ry ,2))  < distStart)	{
   			start = i;
            distStart=sqrt( pow( nodes[start].x - rx ,2) + pow( nodes[start].y - ry ,2)) ;
         }
   		
   		if( sqrt(pow( nodes[i].x - lx ,2) + pow( nodes[i].y - ly ,2)) <  distGoal){
   			goal = i;
            distGoal=sqrt(pow( nodes[goal].x - lx ,2) + pow( nodes[goal].y - ly ,2) );
         }
   }
   printf("Camino de %d a %d\n",start,goal);
   dijkstra_algorithm (goal ,start); // Se pasan al reves para no tener que voltear la lista resultante.
   printf("termina algoritmo\n");
   padre = start;
   i = 0;

   while( padre != -1)
   {
      printf("%d   ",padre);
   	 steps[i].node = nodes[padre].num_node;
   	 steps[i].x = nodes[padre].x;
   	 steps[i].y = nodes[padre].y;
       i++;
   	 padre = nodes[padre].parent;
   }
	return 0;
} 