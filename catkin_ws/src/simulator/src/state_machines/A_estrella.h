#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ros/package.h>

//modificando codigo dijkstra.h del simulador, utilizando las mismas estructuras y funciones auxiliares de los naodos

void A_estrella_algorithm(int D ,int L)
{
   /*
      D = Nodo Inicial
      L = Nodo Final
      Y = Totalmente expandido
      N = Nuevo (Nodo sin padre ni acumulado)
   */

   int menor,flagOnce;
   int contador = 0;
   float h=0,g=0;
   int j,next_node=-1;
   float min=-1;
   nodes[D].acumulado = 0;

   //para nodo inicial
   //tomar todos los nodos conectados
   //elegir el que tenga menor costo y distancia a destino
   //repetir
   printf("de %d a %d\n",D,L);
   while( nodes[L].flag != 'Y')
   {  
      min=-1;
      menor=-1;
      for(j = 0 ; j < nodes[D].num_conections; j++){
         next_node=nodes[D].conections[j].node;
         printf("%d  %c ",next_node,nodes[ next_node].flag);
         if(next_node==L){
            nodes[next_node].flag='Y';
            nodes[next_node].parent = D;
            break;
         }else  if( nodes[ next_node].flag != 'Y'){
            //distancia euclidiana de siguiente nodo a destino
            h=(pow( nodes[next_node].x - nodes[L].x ,2) + pow( nodes[next_node].y - nodes[L].y ,2))/4;
            //costo acomulado
            g=nodes[D].acumulado+(pow( nodes[next_node].x - nodes[D].x ,2) + pow( nodes[next_node].y - nodes[D].y ,2))/*nodes[D].conections[j].cost+*/;
            printf("h+g<min   %f %f\n",h+g,min);
            if((h+g)<min || min<0){
               menor=next_node;
               min=h+g;
            }
            if (nodes[next_node].acumulado > g+h){
               nodes[next_node].acumulado = g+h;
               nodes[next_node].parent = D;
               nodes[next_node].flag = 'P';
            }
         }  
      }
      if(menor ==-1){
         printf("error en menor   %d    ",nodes[D].num_conections);
         break;
      }
      nodes[D].flag = 'Y';
      D = menor;
      printf("\nNuevo D: %d ,Destino L: %d\n",D,L);
   }
}



int A_estrella(float rx ,float ry ,float lx ,float ly, char *world_name,step *steps )
{
   char archivo[150];
   int i;
   int start = 0;
   int goal = 0;
   int padre;
   std::string paths = ros::package::getPath("simulator");
   strcpy(archivo,paths.c_str());
   strcat(archivo,"/src/data/");
   strcat(archivo,world_name);
   strcat(archivo,"/");
   strcat(archivo,world_name);
   strcat(archivo,".top");

   for(i = 0; i < MUM_NODES; i++)
   {
      nodes[i].flag='N';
      nodes[i].num_conections = 0;
      nodes[i].parent = -1;
      nodes[i].acumulado = 0;
   }
 
   num_nodes=read_nodes(archivo); // Se lee el arcivo .top

   //nodos mas cercanos a punto inicial y final
   for(i = 1; i < num_nodes; i++)
   {
   		if( pow( nodes[i].x - rx ,2) + pow( nodes[i].y - ry ,2) <  pow( nodes[start].x - rx ,2) + pow( nodes[start].y - ry ,2) )	
   			start = i;
   		
   		if( pow( nodes[i].x - lx ,2) + pow( nodes[i].y - ly ,2) < pow( nodes[goal].x - lx ,2) + pow( nodes[goal].y - ly ,2)  )
   			goal = i;
   }

   A_estrella_algorithm (goal ,start); // Se pasan al reves para no tener que voltear la lista resultante.
   
   padre = start;
   i = 0;

   while( padre != -1)
   {
   	 steps[i].node = nodes[padre].num_node;
   	 steps[i].x = nodes[padre].x;
   	 steps[i].y = nodes[padre].y;
       i++;
   	 padre = nodes[padre].parent;
   }
	return 0;
} 


