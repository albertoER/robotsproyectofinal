/********************************************************
 *                                                      *
 *                                                      *
 *      campo_potencial.h                            	*
 *                                                      *
 *		Alberto Embarcadero Ruiz			           	*
 *		UNAM                            				*
 *		25-03-2020                                      *
 *                                                      *
 ********************************************************/



#define PF_THRESHOLD 50
struct vec2{
    float x;
    float y;
};
typedef struct vec2 vec2;

// State Machine 
int campo_potencial(float intensity, float*  lights,int obs ,movement *movements  
                    ,int *next_state ,float Mag_Advance ,float max_twist,float sensor_num
                    ,float* sensors,float sensor_max,float laser_origin,float laser_range,float* advance,vec2* last,float robot_x,float robot_y){

    int state = *next_state;
    int result=0;
    int i=0;
    vec2 F_tot,F_rep,F_atr;
    int e1=5,n=30;
    float angle=laser_origin;
    float alpha=laser_range/sensor_num;//angulo entre sensores

    //direccion de la luz
    int dest=0;
    for(i=1;i<8;++i){
        if(lights[dest]<lights[i]){
                dest=i;
        }
    }  

    printf("Present State: %d \n", state);
    printf("intensity %f obstacles %d dest %d\n",intensity,obs,dest);
    
    switch ( state ) {

            case 0:

                if (intensity > PF_THRESHOLD){
                    *movements=generate_output(STOP,Mag_Advance,max_twist);
                    printf("\n **************** Reached light source ******************************\n");
                    *next_state = 0;
                    result = 1;
                } else{
                    if(dest>4){
                        *movements=generate_output(RIGHT,Mag_Advance,M_PI_4*(8-dest));
                    }else{
                        *movements=generate_output(LEFT,Mag_Advance,M_PI_4*dest);
                    }
                    *next_state = 1;
                }

                break;

            case 1:    
                    F_rep.x=0;
                    F_rep.y=0;
                    angle=laser_origin;
                    for(i=0;i<sensor_num;++i){
                        F_rep.x+=(sensor_max-sensors[i])*cos(angle);
                        F_rep.y+=(sensor_max-sensors[i])*sin(angle);
                        angle+=alpha;
                    }
                    F_atr.x=cos(M_PI_4*dest)*(e1);
                    F_atr.y=sin(M_PI_4*dest)*(e1);
                    //suma repulsion y atracion
                    F_tot.x=F_atr.x-n*(F_rep.x);
                    F_tot.y=F_atr.y-n*(F_rep.y);
                    *advance=fmin(Mag_Advance,sqrt(pow(F_tot.x,2)+pow(F_tot.y,2)));
                    *movements=generate_output(LEFT,Mag_Advance,atan(F_tot.y/F_tot.x));
                    if(dest>2 && dest< 6){
                        *next_state = 0;
                    }else{
                        *next_state = 2;
                    }

                break;
            case 2:
                if (intensity > PF_THRESHOLD){
                    *movements=generate_output(STOP,Mag_Advance,max_twist);
                    printf("\n **************** Reached light source ******************************\n");
                    *next_state = 0;
                    result = 1;
                } else{
                 *movements=generate_output(FORWARD,(*advance),0);
                    if((*last).x==robot_x && (*last).y==robot_y){
                        *next_state=3;
                    }else{
                        *next_state = 1;
                    }
                    (*last).x=robot_x;
                    (*last).y=robot_y;
                }
                break;

            case 3: 
                *movements=generate_output(BACKWARD,Mag_Advance,max_twist);
                *next_state = 4;
                break;
            case 4:
                *movements=generate_output(BACKWARD,Mag_Advance,max_twist);
                *next_state = 5;
                break;
            case 5:
                *movements=generate_output(LEFT,Mag_Advance,max_twist);
                *next_state = 6;
                break;

            case 6:
                *movements=generate_output(LEFT,Mag_Advance,max_twist);
                *next_state = 7;
                break;

            case 7: 
                *movements=generate_output(FORWARD,Mag_Advance,max_twist);
                *next_state = 1;
                break;

            default:
                *movements=generate_output(STOP,Mag_Advance,max_twist);
                *next_state = 0;
                break;                
    }

    printf("Next State: %d \n", *next_state);
    return result;
}



                 
